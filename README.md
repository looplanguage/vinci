# Vinci

---

This is the main source code repository for [Vinci](https://looplang.org/docs/internal/vinci) the parser library for 
[Arc](https://looplang.org/docs/internal/arc).

## Get Started
To use this library just add it to your Cargo.toml dependencies:

```toml
[dependencies]
vinci = { git = "https://gitlab.com/looplanguage/vinci" }
```

Now it is usable like this:
```rust
use vinci::parse;

fn main() {
    let result = parse("String here!");
}
```
